<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $user = User::updateOrCreate(["id" => 1], [
            "name" => "Admin",
            "email" => "admin@sayrunjah.com",
            "password" => bcrypt("admin#123")
        ]);
    }
}
