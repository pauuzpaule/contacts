<?php

namespace App\Http\Controllers;

use App\ContactImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function uploadFile(Request $request)
    {
       $data = Excel::toArray([], $request->file('file'));

        $cleanData = collect($data)->flatten()->filter(function ($item){
            return $item != null;
        })->unique()->map(function ($item) {
            //return $item;
            return $this->reformatNumber(strval($item));
        });

        $validContacts = [];
        $invalidContacts = [];
        foreach ($cleanData as $val) {
            if(strlen($val) == 12) {
                $validContacts[] = $val;
            }else {
                $invalidContacts[] = $val;
            }

        }

        $ranStr = $this->generateRandomString(8);
        $filePath = public_path("contacts_{$ranStr}.txt");
        $myfile = fopen($filePath, "a") or die("Unable to open file!");

        fwrite($myfile, "Valid Contacts\n");
        fwrite($myfile, "-------------------");
        fwrite($myfile, "\n");

        foreach ($validContacts as $c) {
            fwrite($myfile, $c."\n");
        }

        fwrite($myfile, "\n Invalid Contacts \n");
        fwrite($myfile, "-------------------");
        fwrite($myfile, "\n");

        foreach ($invalidContacts as $c) {
            fwrite($myfile, $c."\n");
        }

        fclose($myfile);

        return response()->download($filePath);


    }


    private function reformatNumber($number)
    {

        if(substr($number, 0, 1) === '0') {
            $number = trim($number);
            $ptn = "/^0/";  // Regex/
            $rpltxt = "+256";  // Replacement string
            return preg_replace($ptn, $rpltxt, $number);
        }else {
            return "256".$number;
        }

    }

    private function generateRandomString($length = 6)
    {
        $alphabet = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $id = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $p = mt_rand(0, $alphaLength);
            $id[] = $alphabet[$p];
        }
        return implode($id);
    }
}




