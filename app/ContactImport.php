<?php


namespace App;


use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class ContactImport implements ToModel
{

    public function model(array $row)
    {
        return [
            'contact'     => $row[0],
        ];
    }
}
