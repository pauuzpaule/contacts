@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">

                    <form method="POST" action="{{route("upload-file")}}" enctype="multipart/form-data">
                        @csrf
                        <div class="custom-file">
                            <input type="file" name="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>

                        <button class="btn btn-primary btm-sm mt-4">Upload</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
